Rails.application.routes.draw do
  get 'pages/index'
  root 'pages#index'

  devise_for :admins
  devise_for :users
end

